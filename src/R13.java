public class R13 {

    public static String Rotate(String word) {
        String encoded = "";
        for(char currentCharacter:word.toCharArray()){
            if       ((currentCharacter >= 'a' && currentCharacter <= 'm') || (currentCharacter >= 'A' && currentCharacter <= 'M') ) currentCharacter += 13;
            else if  ((currentCharacter >= 'n' && currentCharacter <= 'z') || (currentCharacter >= 'N' && currentCharacter <= 'Z')) currentCharacter -= 13;
            encoded += currentCharacter;
        }
        return encoded;
    }

}
